var dayjs = require("dayjs");

export const formatBasicDay = (val) => {
  if (!val) {
    return null;
  }
 
  return dayjs(val).format("DD / MM / YYYY");
}