export default {
  head: {
    __dangerouslyDisableSanitizers: ['script', 'noscript'],
    htmlAttrs: {
      lang: 'en'
    },
    title: 'bobaho',
    meta: [{
      charset: 'utf-8'
    },
    {
      name: 'theme-color',
      content: '#01234E'
    },
    {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
    },
    {
      hid: 'description',
      name: 'description',
      content: ''
    },
    {
      name: 'msapplication-TileColor',
      content: '#2b5797'
    },
    {
      name: 'theme-color',
      content: '#ffffff'
    },
    ],
    link: [
      {
        rel: 'apple-touch-icon',
        sizes: "180x180",
        href: '/apple-touch-icon.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: "32x32",
        href: '/favicon-32x32.png'
      },

      {
        rel: 'icon',
        type: 'image/png',
        sizes: "16x16",
        href: '/favicon-16x16.png'
      },
      {
        rel: 'manifest',
        href: '/site.webmanifest'
      },
      {
        rel: 'mask-icon',
        href: '/safari-pinned-tab.svg',
        color: "#03306b"
      },
    ],
    script: [
      {
        src: 'https://code.jquery.com/jquery-3.5.1.min.js',
        integrity: 'sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=',
        crossorigin: 'anonymous'
      },
      {
        src: 'https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js',
        integrity: 'sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN',
        crossorigin: 'anonymous'
      },
      {
        src: 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js',
        integrity: 'sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s',
        crossorigin: 'anonymous'
      },
    ], // ---> Comment if dont use  jquery and bootstrap js
  },


  // loading: '~/components/commons/loading/GlobalLoading.vue',

  router: {
    middleware: ['locale', 'checkAgeGate']
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    'bootstrap/scss/bootstrap.scss',
    '~/assets/scss/main.scss',
  ],

  plugins: [
    '~plugins/config',
    '~plugins/i18n',
    '~plugins/axios',
    '~plugins/api',
    '~plugins/vuelidate',
    '~plugins/vue-select',
    { src: '~plugins/vue-pagination.js', mode: 'client' },
    '~/plugins/vue-notify',
  ],

  components: true,

  buildModules: [
    '@nuxtjs/style-resources',
    '@aceforth/nuxt-optimized-images',
    '@nuxtjs/pwa',
  ],
  optimizedImages: {
    optimizeImages: true,
    handleImages: ['jpeg', 'png', 'svg', 'webp'],
    optimizeImages: true,
    optimizeImagesInDev: false,
    defaultImageLoader: 'img-loader',
    mozjpeg: {
      quality: 10,
    },
    optipng: {
      optimizationLevel: 7,
      buffer: true
    },
    pngquant: {
      speed: 11,
      quality: [0, 0.8],
      input: 'Buffer'
    },
    webp: {
      preset: 'default',
      quality: 95,
      alphaQuality: 0,
      method: 0,
      lossless: true,
      buffer: true
    },
  },

  modules: [
    'cookie-universal-nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/device',
    '@nuxtjs/gtm',
    '@luxdamore/nuxt-prune-html'
  ],

  gtm: {
    id: 'GTM-MRP7NWM'
  },

  // facebook: {
  //   /* module options */
  //   track: 'PageView',
  //   pixelId: '203470740828831',
  //   autoPageView: true,
  //   disabled: false,
  //   // debug: true
  // },

  // Module config
  pruneHtml: {
    hideErrorsInConsole: false,
    hideGenericMessagesInConsole: false, // Disabled in production
    enabled: false, // Disabled in dev-mode due to the hot reload (is client-side)
    selectors: [ // Css selectors to prune
      'link[rel="preload"][as="script"]',
      'script:not([type="application/ld+json"])',
    ],
    selectorToKeep: null, // Disallow pruning of scripts with this class, N.B.: this selector will be appended to every selectors, `ex. script:not([type="application/ld+json"]):not(__VALUE__)`
    script: [], // Inject custom scripts only for matched UA (BOTS-only)
    link: [], // Inject custom links only for matched UA (BOTS-only)
    cheerio: { // It use Cheerio under the hood, so this is the config passed in the cheerio.load() method
      xmlMode: false,
    },
    ignoreBotOrLighthouse: false, // Remove selectors in any case, not depending on Bot or Lighthouse
    isBot: true, // Remove selectors if is a bot
    isLighthouse: true, // Remove selectors if match the Lighthouse UserAgent
    matchUserAgent: null, // Remove selectors if this userAgent is matched, either as String or RegExp (a string will be converted to a case-insensitive RegExp in the MobileDetect library)
    hookRenderRoute: true, // Activate the prune during the `hook:render:route`
    hookGeneratePage: true, // Activate the prune during the `hook:generate:page`
    lighthouseUserAgent: 'lighthouse', // Value of the Lighthouse UserAgent, either as String or RegExp (a string will be converted to a case-insensitive RegExp in the MobileDetect library)
    headerName: 'user-agent', // Value of a custom header name passed from a Lambda Edge function, or similar
  },


  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    // vendor: ['bootstrap'],
    extractCSS: true,
    extractCSS: {
      ignoreOrder: true
    },
    splitChunks: {
      layouts: true
    },
    plugins: [
      // set shortcuts as global for bootstrap
      // new webpack.ProvidePlugin({
      //   $: 'jquery',
      //   jQuery: 'jquery',
      //   'window.jQuery': 'jquery'
      // })
    ],
    extend(config, { isDev, isClient, loaders: { vue } }) {
      if (isClient) {
        vue.transformAssetUrls.img = ['data-src', 'src']
        vue.transformAssetUrls.source = ['data-srcset', 'srcset']
      }

      config.module.rules.push({
        test: /\.(ogg|mp3|wav|mpe?g)$/i,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]'
        }
      })
    },
    // terser: {
    //   // https://github.com/terser/terser#compress-options
    //   terserOptions: {
    //     compress: {
    //       drop_console: true
    //     }
    //   }
    // }
  },
  pwa: {
    manifest: {
      name: 'BRING ON THE ONG WITH TIGER',
      lang: 'en',
      background_color: '#6a0912',
      theme_color: '#6a0912',
    }
  },
  styleResources: {
    scss: [
      '~/assets/scss/_variables.scss',
      '~/assets/scss/mixins/mixins.scss',
    ]
  },
  server: {
    port: 3000, // default: 3000
    host: '0.0.0.0', // default: localhost
  },
}
