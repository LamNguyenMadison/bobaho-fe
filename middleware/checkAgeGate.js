export default async ({ store, redirect, route, app }) => {
  if (route.query && route.query.media && route.name === 'index') {
    await store.commit('age-gate/VERIFY_AGE', true);
    return true;
  }

  const excludeName = ['privacy-policy', 'terms-condition']; 
  if (excludeName.includes(route.name)) return true;

  const query = route.query;

  if( query && query['age-gate']) {
    await store.commit('age-gate/VERIFY_AGE', true);
  }

  return true;
}