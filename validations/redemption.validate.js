import {
	required,
	email
} from "vuelidate/lib/validators";

const nircNumber = /^[0-9]*$/;

const validNric = (val) => {
	if (val) {
		const test = nircNumber.test(val);

		if (!test) return false;

		if (val.length !== 12) return false;

		return true;
	}
	return false;
}

const validNricAge = (val) => {
	if (val) {
		//check age
		let age = parseInt(val.substring(0, 2));
		if (age < 21) return false;

		return true;
	}
	return false;
}

const minAge = (val) => {
	if (val) {
		const age = val.slice(0, 2);
		if (age < 21) return false;

		return true;
	}
	return false;
}

const phoneNumber = (val) => {
	if (val) {
		if (val.slice(0, 3) !== '+60') return false;
		const phoneRegex = /^[0-9]*$/;
		const phoneWithoutCode = val.replace('+60', '');
		const test = phoneRegex.test(phoneWithoutCode);
		if (!test) return false;
		if (val.length > 16 || val.length < 10) return false;
		return true;
	}
	return false;
}

const duplicateNric = () => {
	return true;
}

const duplicatePhoneNumber = () => {
	return true;
}

export const redemptionFormValidate = {
	fullname: {
		required,
	},
	nric: {
		required,
		validNric,
		validNricAge,
		duplicateNric
	},
	mobile: {
		required,
		phoneNumber,
		duplicatePhoneNumber
	},
	outlet: {
		required,
	},
	email: {
		required,
		email
	}
}