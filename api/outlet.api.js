export default (axios, resource) => ({
  getOutlets(params) {
    return axios.get(resource.getOutlets, {
      params
    });
  },
  getOutletsPromos(params) {
    return axios.get(resource.getOutletsPromos, {
      params
    });
  },
  getStates() {
    return axios.get(resource.getStates);
  },
  getDistricts(params) {
    return axios.get(resource.getDistricts, {
      params
    });
  },
});