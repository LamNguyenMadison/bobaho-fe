export default (axios, resource) => ({
  getProducts() {
    return axios.get(resource.getProducts);
  },
  getGifts(params) {
    return axios.get(resource.getGiftByProduct, {
      params
    });
  },
  postRedeem(payload) {
    const headers = {
      'Content-Type': 'application/json'
    };

    return axios.post(resource.postRedeem, payload, {
      headers
    });
  },
});