export default (axios, resource) => ({
  ongTeamSubmit(payload) {
    return axios.post(resource.ongTeamSubmit, payload);
  },
});