export default ($axios, resource) => ({
  verifyMedia(payload) {
    return $axios.post(resource.verifyMedia, payload);
  },
  getBrands() {
    return $axios.get(resource.getBrands);
  },
  getPrizes(payload) {
    return $axios.get(resource.getPrizes, {
      params: payload
    });
  },
  getTier() {
    return $axios.get(resource.getTier);
  },
  getUserByClaim(code) {
    return $axios.get(resource.userByClaim.replace(':code', code));
  },
  verifyMediaClaim(payload) {
    return $axios.post(resource.verifyMediaClaim, payload);
  },
  verifyTier2() {
    return $axios.post(resource.verifyTier2);
  },
  getTierDetail(tier) {
    return $axios.get(resource.getTierDetail + tier);
  },
  gameLogin(payload) {
    return $axios.post(resource.gameLogin, payload);
  },
  gamePlay(payload) {
    return $axios.post(resource.gamePlay, payload);
  },
  gameReward(payload) {
    return $axios.post(resource.gameReward, payload);
  },
  verifyLimit(payload) {
    return $axios.post(resource.verifyLimit, payload);
  },
})
