export default (axios, resource) => ({
  getPromotionTypes() {
    return axios.get(resource.getPromotionTypes);
  },
  getPromotions(params) {
    return axios.get(resource.getPromotions, {
      params
    });
  },
  getPromotionDetail(id) {
    return axios.get(resource.getPromotionDetail.replace(':id', id));
  },
});