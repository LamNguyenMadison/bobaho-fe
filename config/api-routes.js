export default {
  api: {
    getInfoByCode: '/info/:code',
    getOutlets: '/outlets',
    getOutletsPromos: '/outlets/promos',
    getStates: '/states',
    getDistricts: '/districts',
    getPromotions: '/promotions',
    getPromotionDetail: '/promotions/:id',
    getProducts: '/products',
    getGiftByProduct: '/products/gifts',
    postRedeem: '/redemptions',
    getPromotionTypes: 'promotions/type',
    getPrizes: '/prizes',
    getTier: '/tiers',
    getBrands: '/brands',
    getTierDetail: '/tiers/detail?type=',
    gameLogin: '/game/login',
    gamePlay: '/game/play',
    gameReward: '/game/reward',
    userByClaim: '/user-info/:code',
    verifyMediaClaim: '/game/verify/code',
    verifyMedia: '/game/verify/media-code',
    verifyTier2: '/game/verify/tier2',
    ongTeamSubmit: '/redemptions-private',
    verifyLimit: '/game/verify/redemption-limit'
  }
}