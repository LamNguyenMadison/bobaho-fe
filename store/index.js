import { VERIFY_COOKIE } from './age-gate/key';

export const actions = {
  nuxtServerInit({ commit }, { req, $appConfig }) {
    const verifyAge = this.$cookies.get(VERIFY_COOKIE);
    const locale = this.$cookies.get('locale');
    
    commit('age-gate/VERIFY_AGE', verifyAge ? true : false);

    if (locale) {
      commit('lang/SET_LOCALE', { locale });
    }
  },
}