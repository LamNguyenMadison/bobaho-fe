// getters
export default {
  promotions: state => state.promotions,
  promotionTypes: state => state.promotionTypes,
}
