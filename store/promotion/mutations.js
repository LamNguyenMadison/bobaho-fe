// mutations
export default {
  SET_PROMOTION(state, promotions) {
    state.promotions = promotions;
  },
  SET_PROMOTION_TYPES(state, promotionTypes) {
    state.promotionTypes = promotionTypes.promotionTypes;
  },
}
