import { SET_PROMOTION, SET_PROMOTION_TYPES } from './mutation-types';

// actions
export default {
  async getPromotions({ commit }, {
    type,
  }) {
    const params = {}
    if (type) params.type = type;

    await this.$api.promotion.getPromotions(params).then(response => {
      commit(SET_PROMOTION, response.data.data);
    }).catch(e => {
      console.log('Request Promotions error: ', e.message);

      if (process.browser) {
        this.$notify({
          group: 'notify',
          text: 'Something went wrong!',
          type: 'error'
        });
      }

      return false;
    });

    return true;
  },
  async getPromotionTypes({ commit }) {
    await this.$api.promotion.getPromotionTypes().then(response => {
      commit(SET_PROMOTION_TYPES, response.data.data);
    }).catch(e => {
      console.log('Request Promotion Types error: ', e.message);

      if (process.browser) {
        this.$notify({
          group: 'notify',
          text: 'Something went wrong!',
          type: 'error'
        });
      }

      return false;
    });

    return true;
  },
}