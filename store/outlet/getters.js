// getters
export default {
  outlets: state => state.outlets,
  districts: state => state.districts,
  regions: state => state.regions,
  promotypes: state => state.promotypes,
  countOutlet: state => state.countOutlet,
  outletsRedemption: state => state.outletsRedemption,
}
