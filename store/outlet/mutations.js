// mutations
export default {
  SET_OUTLET(state, outlets) {
    state.countOutlet = outlets.count;
    state.outlets = outlets.outlets;
  },
  SET_DISTRICTS(state, districts) {
    state.districts = districts;
  },
  SET_REGIONS(state, regions) {
    state.regions = regions;
  },
  SET_PROMOTYPE(state, promotypes) {
    state.promotypes = promotypes;
  },
  SET_OUTLET_REDEMPTION(state, outlets) {
    state.outletsRedemption.push(...outlets.outlets);
  },
}
