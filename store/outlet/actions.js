import { SET_DISTRICTS, SET_OUTLET, SET_REGIONS, SET_PROMOTYPE, SET_OUTLET_REDEMPTION } from './mutation-types';

// actions
export default {
  async getOutlets({ commit }, {
    stateId,
    districtId,
    outletPromoId,
    page
  }) {
    const params = {}
    if (stateId) params.stateId = stateId;
    if (districtId) params.districtId = districtId;
    if (outletPromoId) params.outletPromoId = outletPromoId;
    if (page) params.page = page;

    await this.$api.outlet.getOutlets(params).then(response => {
      commit(SET_OUTLET, response.data.data);
    }).catch(e => {
      console.log('Request Outlet error: ', e.message);

      if (process.browser) {
        this.$notify({
          group: 'notify',
          text: 'Something went wrong!',
          type: 'error'
        });
      }

      return false;
    });

    return true;
  },

  async getOutletsData({ commit }, {
    stateId,
    districtId,
    outletPromoId,
    page
  }) {
    const params = {}
    if (stateId) params.stateId = stateId;
    if (districtId) params.districtId = districtId;
    if (outletPromoId) params.outletPromoId = outletPromoId;
    if (page) params.page = page;

    const response = await this.$api.outlet.getOutlets(params).then(response => {
      commit(SET_OUTLET_REDEMPTION, response.data.data);
      return response.data.data;
    }).catch(e => {
      console.log('Request Outlet error: ', e.message);

      if (process.browser) {
        this.$notify({
          group: 'notify',
          text: 'Something went wrong!',
          type: 'error'
        });
      }
      return false;
    });
    return response;
  },

  async getStates({ commit }) {
    await this.$api.outlet.getStates().then(response => {
      commit(SET_REGIONS, response.data.data);
    }).catch(e => {
      console.log('Request Outlet states error: ', e.message);

      if (process.browser) {
        this.$notify({
          group: 'notify',
          text: 'Something went wrong!',
          type: 'error'
        });
      }

      return false;
    });

    return true;
  },
  async getDistricts({ commit }, {
    stateId
  }) {
    const params = {}
    if (stateId) params.stateId = stateId;
    await this.$api.outlet.getDistricts(params).then(response => {
      commit(SET_DISTRICTS, response.data.data);
    }).catch(e => {
      console.log('Request Outlet districts error: ', e.message);

      if (process.browser) {
        this.$notify({
          group: 'notify',
          text: 'Something went wrong!',
          type: 'error'
        });
      }

      return false;
    });

    return true;
  },
  async getPromotype({ commit }, {
    stateId,
    districtId
  }) {
    const params = {}
    if (stateId) params.stateId = stateId;
    if (districtId) params.districtId = districtId;

    await this.$api.outlet.getOutletsPromos(params).then(response => {
      // console.log(response.data);
      commit(SET_PROMOTYPE, response.data.data);
    }).catch(e => {
      console.log('Request Outlet promo error: ', e.message);

      return false;
    });

    return true;
  },
}