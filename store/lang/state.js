import appConfig from '~/config';

// state
export default () => ({
  locale: appConfig.locale,
  locales: appConfig.locales
});
