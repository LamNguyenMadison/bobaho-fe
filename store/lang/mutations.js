// mutations
export default {
  SET_LOCALE(state, { locale }) {
    state.locale = locale;
    this.$loadMessages(locale);
  }
}
