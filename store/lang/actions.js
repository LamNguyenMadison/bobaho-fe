import { SET_LOCALE } from './mutation-types';

// actions
export default {
  setLocale ({ commit }, { locale }) {
    commit(SET_LOCALE, { locale });

    this.$cookies.set('locale', locale, { expires: 365 });
  }
}