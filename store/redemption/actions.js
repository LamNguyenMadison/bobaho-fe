import { SET_RESULT_REDEEM } from './mutation-types';

// actions
export default {
  async submitRedeem({ commit }, payload) {
    try {
      const response = await this.$api.redeem.postRedeem(payload);
      commit(SET_RESULT_REDEEM, response.data.data);
      commit('RESET_PROCESS');
      return response.data.data;
    } catch (e) {
      throw e;
    };
  },
}