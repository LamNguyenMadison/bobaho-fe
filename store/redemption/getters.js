// getters
export default {
  personalForm: state => state.personalForm,
  purchaseDetailForm: state => state.purchaseDetailForm,
  redemptionResult: state => state.redemptionResult,
}
