// state
export default () => ({
  personalForm: null,
  purchaseDetailForm: null,
  redemptionResult: false,
});
