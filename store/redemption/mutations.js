// mutations
export default {
  STORE_PERSONAL_STEP(state, personalForm) {
    state.personalForm = personalForm;
  },
  STORE_PURCHASE_STEP(state, purchaseDetailForm) {
    state.purchaseDetailForm = purchaseDetailForm;
  },
  RESET_PROCESS(state) {
    state.personalForm = null;
    state.purchaseDetailForm = null;
  },
  SET_RESULT_REDEEM(state, result) {
    state.redemptionResult = result;
  },
}
