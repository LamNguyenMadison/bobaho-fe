import { VERIFY_COOKIE } from './key';

// mutations
export default {
  VERIFY_AGE(state, status) {
    state.verifyAge = status;
    this.$cookies.set(VERIFY_COOKIE, status);
  },
}